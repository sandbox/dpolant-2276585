<?php

/*
 * Implements hook_default_rules_configuration().
 */
function commerce_sapi_popular_products_default_rules_configuration() {
  // Rule component: handle line item product index tracking update if the 
  // line item order is not a shopping cart.
  $parameters = array(
    'commerce_line_item' => array(
      'label' => t('Line item'),
      'type' => 'commerce_line_item'
    )
  );
  $rule = rule($parameters);
  $rule->label = t('Handle conditional line item product reindex queueing');
  $rule->tags = array('Commerce SAPI popular products');
  $rule->active = TRUE;
  
  $rule
    ->condition(rules_condition('data_is_empty', array(
      'data:select' => 'commerce-line-item:order'
    ))->negate())
    ->condition(rules_condition('commerce_order_is_cart', array(
      'commerce_order:select' => 'commerce-line-item:order'
    ))->negate())
    ->action('commerce_sapi_popular_products_track_line_item_product_display_change', array(
      'commerce_line_item:select' => 'commerce-line-item'
    ));
  
  $rules['commerce_sapi_popular_products_queue_line_item_product_reindex'] = $rule;

  // Reaction rule: handle line item product tracking on line item insert.
  $rule = rules_reaction_rule();

  $rule->label = t('Queue catalog index item updates (line item delete)');
  $rule->tags = array('Commerce SAPI popular products');
  $rule->active = TRUE;

  $rule
    ->event('commerce_line_item_delete')   
    ->action('component_commerce_sapi_popular_products_queue_line_item_product_reindex', array(
      'commerce_order:select' => 'commerce-order',
    ));
  
  $rules['commerce_sapi_popular_products_queue_reindex_line_item_insert'] = $rule;  
  
  // Reaction rule: handle line item product tracking when the order id goes 
  // from null to set, as it does when an admin creates line items manually.
  $rule = rules_reaction_rule();

  $rule->label = t('Queue catalog index item updates (line item update)');
  $rule->tags = array('Commerce SAPI popular products');
  $rule->active = TRUE;

  $rule
    ->event('commerce_line_item_update')
    ->condition(rules_condition('data_is_empty', array(
      'data:select' => 'commerce-line-item:order-id'
    ))->negate())
    ->condition('data_is_empty', array(
      'data:select' => 'commerce-line-item-unchanged:order-id'  
    ))
    ->action('component_commerce_sapi_popular_products_queue_line_item_product_reindex', array(
      'commerce_order:select' => 'commerce-order',
    ));
  
  $rules['commerce_sapi_popular_products_queue_reindex_line_item_update'] = $rule;
  
  // Reaction rule: handle line item product tracking when on checkout complete.
  $rule = rules_reaction_rule();

  $rule->label = t('Queue catalog index item updates (checkout complete)');
  $rule->tags = array('Commerce SAPI popular products');
  $rule->active = TRUE;  
  
  $rule
    ->event('commerce_checkout_complete')
    ->action(rules_loop(array(
      'list:select' => 'commerce-order:commerce-line-items',
      'item:var' => 'commerce_line_item',
      'item:label' => t('Line item'),
      'item:type' => 'commerce_line_item'))
      ->action('commerce_sapi_popular_products_track_line_item_product_display_change', array(
        'commerce_line_item:select' => 'commerce-line-item'
      ))
    );
  
  $rules['commerce_sapi_popular_products_queue_reindex_checkout_complete'] = $rule;
  
  return $rules;
}
