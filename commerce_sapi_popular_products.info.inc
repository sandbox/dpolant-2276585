<?php

/*
 * Implements hook_entity_property_info_alter().
 */
function commerce_sapi_popular_products_entity_property_info_alter(&$info) {
  foreach (field_info_fields() as $field) {
    if ($field['module'] != 'commerce_product_reference') {
      continue;
    }
    
    $field_name = $field['field_name'];
    foreach ($field['bundles'] as $entity_type => $bundles) {
      foreach ($bundles as $bundle) {
        // Add another property for "number of times ordered"
        $times_ordered_label = t('Times ordered for products on bundle @bundle through field @field', array(
            '@field' => $field_name,
            '@bundle' => $bundle,
        ));        
        $info[$entity_type]['properties'][$field_name . '_' . $bundle . '_order_count'] = array(
          'label' => $times_ordered_label,
          'description' => $times_ordered_label,
          'getter callback' => 'commerce_sapi_popular_products_product_times_ordered_getter',
          'field_name' => $field_name,
          'type' => 'integer',
          'host type' => $entity_type
        );
      }
    }
  }
}
