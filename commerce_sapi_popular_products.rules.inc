<?php

/*
 * Implements hook_rules_action_info().
 */
function commerce_sapi_popular_products_rules_action_info() {
  $actions['commerce_sapi_popular_products_track_line_item_product_display_change'] = array(
    'label' => t('Queue product displays related to a line item for reindexing'),
    'group' => t('Commerce SAPI popular products'),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line item'),
        'wrapped' => TRUE
      )
    )
  );
  
  return $actions;
}

/*
 * Implements hook_rules_condition_info().
 */
function commerce_sapi_popular_products_rules_condition_info() {

}

/*
 * Rules action callback: track search api index changes for product display 
 * nodes that reference products found in a line item.
 */
function commerce_sapi_popular_products_track_line_item_product_display_change($line_item_wrapper) {
  if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
    // Load nodes that reference this product.    
    $query = new EntityFieldQuery;
    $results = $query
      ->entityCondition('entity_type', 'node')
      ->fieldCondition('field_product', 'product_id', $line_item_wrapper->commerce_product->product_id->value())
      ->execute();
    
    // Mark any nodes referencing the products on this order as "dirty" so that
    // they will be reindexed.
    if (!empty($results['node'])) {
      search_api_track_item_change('node', array_keys($results['node']));
    }
  }
}
